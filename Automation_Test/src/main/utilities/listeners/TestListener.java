package listeners;

import handles.DriverHandle;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;


public class TestListener implements ITestListener {

    public String getTestName(ITestResult result) {
        return result.getName();
    }

    public String getTestDescription(ITestResult result) {
        return result.getMethod().getDescription();
    }

    @Override
    public void onFinish(ITestContext result) {

    }

    @Override
    public void onStart(ITestContext result) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("Success Test case: " + getTestName(result));

    }

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.println("Failure Test case: " + getTestName(result));
        WebDriver driver = DriverHandle.getDriver();
        ReportListener.saveScreenshotPNG(driver);
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        System.out.println("Skipped Test case: " + getTestName(result));

    }

    @Override
    public void onTestStart(ITestResult result) {
        System.out.println(getTestName(result)+ " test case started");

    }
}