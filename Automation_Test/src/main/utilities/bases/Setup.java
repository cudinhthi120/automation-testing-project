package bases;

import global_values.GlobalValue;
import handles.DriverHandle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.*;

//@Listeners(TestListener.class)
public class Setup {
    @BeforeMethod
    @Parameters({"Browser Type"})
    public void createDriver(@Optional("chrome") String browserName) {
        WebDriver driver = setupBrowser(browserName);
        DriverHandle.setDriver(driver);
    }

    public WebDriver setupBrowser(String browserName) {
        return switch (browserName.trim().toLowerCase()) {
            case "chrome" -> initChromeDriver();
            case "firefox" -> initFirefoxDriver();
            case "edge" -> initEdgeDriver();
            default -> {
                System.out.println("Browser: " + browserName + " is invalid, Launching Chrome as browser of choice...");
                yield initChromeDriver();
            }
        };
    }

    private WebDriver initChromeDriver() {
        WebDriver driver;

        System.out.println("Launching Chrome browser...");
        ChromeOptions options = new ChromeOptions();
        if (GlobalValue.HEADLESS) {
            options.addArguments("--headless=new");
            options.addArguments("window-size=1800,900");
        }
        options.addArguments("--remote-allow-origins=*");

        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        return driver;
    }

    private WebDriver initEdgeDriver() {
        WebDriver driver;

        System.out.println("Launching Edge browser...");
        EdgeOptions options = new EdgeOptions();
        if (GlobalValue.HEADLESS) {
            options.addArguments("--headless=new");
            options.addArguments("window-size=1800,900");
        }

        driver = new EdgeDriver(options);
        driver.manage().window().maximize();
        return driver;
    }

    private WebDriver initFirefoxDriver() {
        WebDriver driver;

        System.out.println("Launching Firefox browser...");
        FirefoxOptions options = new FirefoxOptions();
        if (GlobalValue.HEADLESS) {
            options.addArguments("--headless");
            options.addArguments("window-size=1800,900");
        }

        driver = new FirefoxDriver(options);
        driver.manage().window().maximize();
        return driver;
    }

    @AfterMethod
    public void closeUI()  {
        if (DriverHandle.getDriver() != null) {
            DriverHandle.quit();
        }
    }
}
