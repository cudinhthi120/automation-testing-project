package global_values;

import handles.PropertiesHandle;

public class GlobalValue {
    public final static String URL = PropertiesHandle.getValueFromConfig("URL");
    public final static Boolean HEADLESS = Boolean.parseBoolean(PropertiesHandle.getValueFromConfig("HEADLESS"));
    public final static String USERNAME = PropertiesHandle.getValueFromConfig("USERNAME");
    public final static String PASSWORD = PropertiesHandle.getValueFromConfig("PASSWORD");
}
