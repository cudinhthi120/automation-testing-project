package keywords;

import handles.DriverHandle;
import io.qameta.allure.Step;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;
import org.testng.Assert;

import java.time.Duration;
import java.util.List;

public class UI {
    public static WebDriver driver() {
        return DriverHandle.getDriver();
    }

    public static Actions actions() {
        return new Actions(driver());
    }

    public static WebElement getWebElement(By element) {
        return driver().findElement(element);
    }

    public static List<WebElement> getWebElements(By element) {
        return driver().findElements(element);
    }

    public static String getTitle() {
        waitForPageLoaded();
        return driver().getTitle();
    }

    public static boolean getCurrentURL(String url) {
        waitForPageLoaded();
        return DriverHandle.getDriver().getCurrentUrl().contains(url);
    }

    public static String getTextByAttribute(By element) {
        waitForElementVisible(element);
        return getWebElement(element).getAttribute("value");
    }

    public static String getText(By element) {
        waitForElementVisible(element);
        return getWebElement(element).getText();
    }

    public static String getTagName(By element) {
        waitForElementVisible(element);
        return getWebElement(element).getTagName();
    }

    public static String getColor(By element) {
        return getWebElement(element).getCssValue("background-color");
    }

    public static String getFontFamily(By element) {
        return getWebElement(element).getCssValue("font-family");
    }

    public static String getFontSize(By element) {
        return getWebElement(element).getCssValue("font-size");
    }

    public static String getFontWeight(By element) {
        return getWebElement(element).getCssValue("font-weight");
    }

    public static String getTextAlign(By element) {
        return getWebElement(element).getCssValue("text-align");
    }

    @Step("Open URL: {0}")
    public static void openURL(String url) {
        DriverHandle.getDriver().get(url);
        waitForPageLoaded();
    }

    @Step("Verify Equals: Actual: {0} with Expected: {1}")
    public static void assertEquals(Object actual, Object expected) {
        waitForPageLoaded();
        Assert.assertEquals(actual, expected, "Not match. '" + actual.toString() + "' and '" + expected.toString() + "'");
    }

    @Step("Set text '{1}' on {0}")
    public static void setText(By element, String text) {
        waitForPageLoaded();
        getWebElement(element).sendKeys(text);
    }

    @Step("Clear field {0}")
    public static void clearField(By element) {
        waitForPageLoaded();
        getWebElement(element).clear();
    }

    @Step("Click on {0}")
    public static void clickElement(By element) {
        waitForPageLoaded();
        waitForElementVisible(element);
        getWebElement(element).click();
        waitForPageLoaded();
    }

    @Step("Click on {1} in list {0}")
    public static void clickElementInList(List<WebElement> listElement, int number) {
        waitForPageLoaded();
        listElement.get(number).click();
    }

    @Step("Right click on {0}")
    public static void rightClick(By element) {
        actions().contextClick(getWebElement(element));
    }
    @Step("Double click on {0}")
    public static void doubleClick(By element) {
        actions().doubleClick(getWebElement(element));
    }

    @Step("Set {1} in dropdown {0}")
    public void selectValueInDropdown(By element, String text) {
        Select dropdownList = new Select(getWebElement(element));
        dropdownList.selectByVisibleText(text);
    }

    public void submit(By element) {
        waitForElementVisible(element);
        getWebElement(element).submit();
    }

    @Step("Check {0} is selected")
    public static Boolean isSelected(By element) {
        waitForElementVisible(element);
        return getWebElement(element).isSelected();
    }
    @Step("Check {0} is displayed")
    public Boolean isDisplayed(By element) {
        waitForElementVisible(element);
        return getWebElement(element).isDisplayed();
    }

    @Step("Check {0} is enabled")
    public Boolean isEnabled(By element) {
        waitForElementVisible(element);
        return getWebElement(element).isEnabled();
    }

    @Step("Switch to alert {0}")
    public void switchToAlert() {
        driver().switchTo().alert();
    }

    @Step("Switch to frame {0}")
    public void switchToFrameByName(String frameName) {
        driver().switchTo().frame(frameName);
    }

    @Step("Switch to frame {0}")
    public void switchToFrameByIndex(int index) {
        driver().switchTo().frame(index);
    }

    @Step("Switch to frame {0}")
    public void switchToFrameByElement(By element) {
        driver().switchTo().frame(getWebElement(element));
    }

    public void switchToDefaultFrame() {
        driver().switchTo().defaultContent();
    }

    @Step("Select all text in {0}")
    public static void selectAllAction(By element) {
        actions().keyDown(getWebElement(element), Keys.CONTROL)
                .sendKeys("a")
                .keyUp(getWebElement(element),Keys.CONTROL )
                .perform();
    }

    @Step("Press DEL on keyboard")
    public static void deleteAction() {
        actions().sendKeys(Keys.DELETE).perform();
    }

    @Step("Press ENTER on keyboard to {0}")
    public static void enterAction(By element) {
        actions().keyDown(getWebElement(element), Keys.ENTER)
                .keyUp(Keys.ENTER)
                .perform();
    }

    public static void deleteAllAction(By element) {
        selectAllAction(element);
        deleteAction();
    }

    public static void waitForElementVisible(By element) {
        try {
            WebDriverWait wait = new WebDriverWait(driver(), Duration.ofSeconds(10));
            wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        } catch (Throwable error) {
            Assert.fail("Can't wait for the element Visible. " + element.toString());
        }
    }

    public static void waitForPageLoaded() {
        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString()
                        .equals("complete");
            }
        };
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(DriverHandle.getDriver(), Duration.ofSeconds(30));
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }
}
