package handles;

import org.openqa.selenium.WebDriver;

public class DriverHandle {
    private static final ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    private DriverHandle() {
    }

    public static WebDriver getDriver() {
        return driver.get();
    }

    public static void setDriver(WebDriver driver) {
        DriverHandle.driver.set(driver);
    }

    public static void quit() {
        DriverHandle.driver.get().quit();
        driver.remove();
    }
}
