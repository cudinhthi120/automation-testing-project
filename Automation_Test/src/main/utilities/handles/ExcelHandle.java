package handles;

import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelHandle {

    private FileInputStream fis;
    private FileOutputStream fileOut;
    private Workbook wb;
    private Sheet sh;
    private Cell cell;
    private Row row;
    private CellStyle cellstyle;
    private Color mycolor;
    private String excelFilePath;
    private final Map<String, Integer> columns = new HashMap<>();

    public void setExcelFile(String ExcelFileName, String SheetName)  {
        try {
            File f = new File("src/main/java/test_data/" + ExcelFileName);

            if (!f.exists()) {
                f.delete();
                System.out.println("File doesn't exist, so created!");
            }
            f.createNewFile();

            fis = new FileInputStream("src/main/java/test_data/" + ExcelFileName);
            wb = WorkbookFactory.create(fis);
            sh = wb.getSheet(SheetName);
            if (sh == null) {
                sh = wb.createSheet(SheetName);
            }

            this.excelFilePath = "src/main/java/test_data/" + ExcelFileName;

            //adding all the column header names to the map 'columns'
            sh.getRow(0).forEach(cell ->{
                columns.put(cell.getStringCellValue(), cell.getColumnIndex());
            });

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public String getCellData(int rowNum, int colNum) {
        try{
            cell = sh.getRow(rowNum).getCell(colNum);
            String CellData = null;
            switch (cell.getCellType()){
                case STRING:
                    CellData = cell.getStringCellValue();
                    break;
                case NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell))
                    {
                        Date date = cell.getDateCellValue();
                        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        CellData = dateFormat.format(date);
                    }
                    else
                    {
                        CellData =  String.valueOf((long)cell.getNumericCellValue());
                    }
                    break;
                case BOOLEAN:
                    CellData = Boolean.toString(cell.getBooleanCellValue());
                    break;
                case BLANK:
                    CellData = "";
                    break;
            }
            return CellData;
        }catch (Exception e){
            return"";
        }
    }

    public List<String> getRowData(int rowNum) {
        Row row = sh.getRow(rowNum); // Lấy hàng cụ thể
        List<String> data = new ArrayList<>();
        if (row != null) {
            int numberOfCells = row.getPhysicalNumberOfCells();
            String[] rowData = new String[numberOfCells];

            for (int cellIndex = 0; cellIndex < numberOfCells; cellIndex++) {
                Cell cell = row.getCell(cellIndex);
                rowData[cellIndex] = getCellDataWithoutSelectColumn(cell);
            }

            // In dữ liệu trong mảng
            Collections.addAll(data, rowData);
        } else {
            System.out.print("Hàng ko tồn tại");
        }
        return data;
    }

    private String getCellDataWithoutSelectColumn(Cell cell) {
        if (cell == null) {
            return "";
        }
        String CellData = null;
        switch (cell.getCellType()){
            case STRING:
                CellData = cell.getStringCellValue();
                break;
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell))
                {
                    Date date = cell.getDateCellValue();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    CellData = dateFormat.format(date);
                }
                else
                {
                    CellData =  String.valueOf((long)cell.getNumericCellValue());
                }
                break;
            case BOOLEAN:
                CellData = Boolean.toString(cell.getBooleanCellValue());
                break;
            case BLANK:
                CellData = "";
                break;
        }
        return CellData;
    }

    // Get data from file excel
    public String getCellData(String columnName, int rowNum)  {
        return getCellData(rowNum, columns.get(columnName));
    }

    // Set data to file excel
    public void setCellData(String text, int rowNum, int colNum) throws Exception {
        try{
            row  = sh.getRow(rowNum);
            if(row ==null)
            {
                row = sh.createRow(rowNum);
            }
            cell = row.getCell(colNum);

            if (cell == null) {
                cell = row.createCell(colNum);
            }
            cell.setCellValue(text);

            fileOut = new FileOutputStream(excelFilePath);
            wb.write(fileOut);
            fileOut.flush();
            fileOut.close();
        }catch(Exception e){
            throw (e);
        }
    }

    public int getLastRowNum() {
        return sh.getLastRowNum();
    }

    //Lấy số dòng có data đang sử dụng
    public int getPhysicalNumberOfRows() {
        return sh.getPhysicalNumberOfRows();
    }

    public int getColumns() {
        try {
            row = sh.getRow(0);
            return row.getLastCellNum();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw (e);
        }
    }

    public Object[][] getExcelDataHashTable(String excelPath, String sheetName, int startRow, int endRow) {
        System.out.println("Excel Path: " + excelPath);
        Object[][] data = null;

        try {
            File f = new File(excelPath);
            if (!f.exists()) {
                try {
                    System.out.println("File Excel path not found.");
                    throw new IOException("File Excel path not found.");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            fis = new FileInputStream(excelPath);

            wb = new XSSFWorkbook(fis);

            sh = wb.getSheet(sheetName);

            int rows = getLastRowNum();
            int columns = getColumns();

            System.out.println("Row: " + rows + " - Column: " + columns);
            System.out.println("StartRow: " + startRow + " - EndRow: " + endRow);

            data = new Object[(endRow - startRow) + 1][1];
            Hashtable<String, String> table = null;
            for (int rowNums = startRow; rowNums <= endRow; rowNums++) {
                table = new Hashtable<>();
                for (int colNum = 0; colNum < columns; colNum++) {
                    table.put(getCellData(0, colNum), getCellData(rowNums, colNum));
                }
                data[rowNums - startRow][0] = table;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

}
