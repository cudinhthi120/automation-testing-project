package handles;

import org.testng.annotations.DataProvider;

import java.io.File;

public class DataHandle {
    ExcelHandle excelHandle;

    public DataHandle() {
        excelHandle = new ExcelHandle();
    }
    @DataProvider(name = "data_provider_login_from_excel_by_row", parallel = true)
    public Object[][] dataLoginHRMFromExcelByRow() {
        return excelHandle.getExcelDataHashTable(System.getProperty("user.dir") + File.separator + "Testdata.xlsx", "LoginTestData", 1, 10);
    }
}
