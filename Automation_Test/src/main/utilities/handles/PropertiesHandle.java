package handles;

import java.io.*;
import java.util.LinkedList;
import java.util.Properties;

public class PropertiesHandle {
    private static Properties properties;
    private static String linkFile;
    private static FileInputStream fileInput;
    private static FileOutputStream fileOutput;
    private static final String configPropertiesFilePath = "src/main/configs.properties";

    public static Properties loadAllFiles() {
        LinkedList<String> files = new LinkedList<>();

        files.add("src/test/resources/configs.properties");

        try {
            properties = new Properties();

            for(String f : files) {
                Properties tempProp =  new Properties();
                linkFile = System.getProperty("user.dir") + File.separator + f;
                fileInput = new FileInputStream((linkFile));
                tempProp.load(fileInput);
                properties.putAll(tempProp);
            }
            return properties;
        } catch (IOException e) {
            return new Properties();
        }
    }

    public static String getValueFromConfig(String key) {
        String value = null;
        try {
            if(fileInput == null) {
                properties = new Properties();
                linkFile = System.getProperty("user.dir") + File.separator + configPropertiesFilePath;
                fileInput = new FileInputStream(linkFile);
                properties.load(fileInput);
                fileInput.close();
            }
            value = properties.getProperty(key);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return value;
    }
}
