package test_scripts.test_cases;

import bases.Setup;
import global_values.GlobalValue;
import handles.ExcelHandle;
import keywords.UI;
import org.testng.Assert;
import org.testng.annotations.*;
import test_scripts.page_objectives.CreatePostPage;
import test_scripts.page_objectives.HomePage;

public class CreatePostTest extends Setup {
     ExcelHandle excelHandle;
//     HomePage homePage;
//     LoginPage loginPage;
//     CreatePostPage createPostPage;
    LoginTest loginTest;


    @BeforeMethod
    public void setUp() {
//        loginPage = new LoginPage();
//        homePage = new HomePage();
//        createPostPage = new CreatePostPage();
        loginTest = new LoginTest();
        excelHandle = new ExcelHandle();
        excelHandle.setExcelFile("Testdata.xlsx", "CreatePostTestData");
        UI.openURL(GlobalValue.URL);
    }

    @Test(priority = 1)
    public void CreatePostTestCase() {
        // Login to home page
        loginTest.loginWithValidAccount();

        // Go to create post page
        HomePage.goToCreatePostPage();

        // Create post flow
        // solution 1
//        createPostPage.createPost(
//                excelHandle.getCellData("City", 1),
//                excelHandle.getCellData("District", 1),
//                excelHandle.getCellData("Ward", 1),
//                excelHandle.getCellData("Street", 1),
//                excelHandle.getCellData("Address Number", 1),
//                excelHandle.getCellData("Living Area", 1),
//                excelHandle.getCellData("Price",1),
//                excelHandle.getCellData("Title",1),
//                excelHandle.getCellData("Description", 1)
//        );

        // solution 2
        UI.clickElement(CreatePostPage.categorySelectBox());
        UI.clickElement(CreatePostPage.fistCategoryChoice());
        UI.clickElement(CreatePostPage.secondCategoryChoice());
        UI.clickElement(CreatePostPage.thirdCategoryChoice());
        UI.clickElement(CreatePostPage.cityDropdown());
        UI.clickElement(CreatePostPage.cityElement(excelHandle.getCellData("City", 1)));
        UI.clickElement(CreatePostPage.districtDropdown());
        UI.clickElement(CreatePostPage.districtElement(excelHandle.getCellData("District", 1)));
        UI.clickElement(CreatePostPage.wardDropdown());
        UI.clickElement(CreatePostPage.wardElement(excelHandle.getCellData("Ward", 1)));
        UI.clickElement(CreatePostPage.streetDropdown());
        UI.clickElement(CreatePostPage.streetElement(excelHandle.getCellData("Street", 1)));
        UI.setText(CreatePostPage.addressNumberField(), excelHandle.getCellData("Address Number", 1));
        UI.clickElement(CreatePostPage.showAddressNumberCheckBox());
        UI.setText(CreatePostPage.livingAreaField(), excelHandle.getCellData("Living Area", 1));
        UI.setText(CreatePostPage.priceField(), excelHandle.getCellData("Price",1));
        UI.setText(CreatePostPage.postTitle(), excelHandle.getCellData("Title",1));
        UI.setText(CreatePostPage.postDescription(), excelHandle.getCellData("Description", 1));
        UI.clickElement(CreatePostPage.nextButton());
        Assert.assertTrue(UI.getCurrentURL("/mua-dich-vu"), "This is not buy services page");
    }
}
