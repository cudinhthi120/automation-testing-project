package test_scripts.test_cases;

import bases.Setup;
import global_values.GlobalValue;
import keywords.UI;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import test_scripts.page_objectives.HomePage;
import test_scripts.page_objectives.LoginPage;
import test_scripts.page_objectives.ManageListingPage;

public class CheckTotalItemInTabAllTest extends Setup {
    LoginPage loginPage;
    HomePage homePage;
    ManageListingPage manageListingPage;

    @BeforeMethod
    public void setUp() {
        loginPage = new LoginPage();
        homePage = new HomePage();
        manageListingPage = new ManageListingPage();
        UI.openURL(GlobalValue.URL);
    }

    @Test
    public void testCheckTotalItemInTabAll() {
        // Login to home page
        loginPage.login("0902883759", "dinhthi3105");

        // Go to management listing page
        homePage.goToPostManagementPage();

        // Check total item in tab All
        manageListingPage.checkTotalItemInTab();
    }
}
