package test_scripts.test_cases;

import bases.Setup;
import global_values.GlobalValue;
import handles.ExcelHandle;
import keywords.UI;
import listeners.TestListener;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import test_scripts.page_objectives.LoginPage;
import test_scripts.page_objectives.RegistrationPage;

@Listeners(TestListener.class)
public class RegisterTest extends Setup {
    LoginPage loginPage;
    ExcelHandle excelHandle;
    RegistrationPage registrationPage;
    @BeforeMethod
    public void setUp() {
        excelHandle = new ExcelHandle();
        loginPage = new LoginPage();
        registrationPage = new RegistrationPage();
        excelHandle.setExcelFile("Testdata.xlsx", "RegisterTestData");
        UI.openURL(GlobalValue.URL);
    }

    @Test (priority = 0, description = "Check password length validation", testName = "Check password length validation")
    public void registerPasswordLengthValidation() {
        // Go to registration page
        loginPage.goToRegisterPage();

        registrationPage.registerPasswordLengthValidation(
                excelHandle.getCellData("Username",4),
                excelHandle.getCellData("Password", 4)
        );

    }

    @Test (priority = 1, description = "Check username length validation")
    public void registerUsernameLengthValidation() {
        // Go to registration page
        loginPage.goToRegisterPage();

        // Register flow
        registrationPage.register(
                excelHandle.getCellData("Username",3),
                excelHandle.getCellData("Password", 3)
        );
    }

    @Test (priority = 2, description = "Check duplicate username")
    public void registerWithDuplicateUserName() {
        // Go to registration page
        loginPage.goToRegisterPage();

        // Register flow
        registrationPage.register(
                excelHandle.getCellData("Username", 2),
                excelHandle.getCellData("Password", 2)
        );
    }

    @Test (priority = 3, description = "Check invalid phone number")
    public void registerWithInvalidPhoneNumber() {
        // Go to registration page
        loginPage.goToRegisterPage();

        // Register flow
        registrationPage.register(
                excelHandle.getCellData("Username",1),
                excelHandle.getCellData("Password", 1)
        );
    }

    @Test (priority = 4, description = "Register without password")
    public void registerWithoutPassword() {
        // Go to registration page
        loginPage.goToRegisterPage();

        // Register flow
        registrationPage.register(
                excelHandle.getCellData("Username",6),
                excelHandle.getCellData("Password", 6)
        );
    }

    @Test (priority = 5, description = "Register without username")
    public void registerWithoutUsername() {
        // Go to registration page
        loginPage.goToRegisterPage();

        registrationPage.register(
                excelHandle.getCellData("Username", 5),
                excelHandle.getCellData("Password", 5)
        );
    }

    @Test (priority = 6, description = "Register with valid information")
    public void registerWithValidInfo() {
        // Go to registration page
        loginPage.goToRegisterPage();

        // Register flow
        registrationPage.register(
                excelHandle.getCellData("Username",8),
                excelHandle.getCellData("Password", 8)
        );
    }
}
