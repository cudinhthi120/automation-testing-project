package test_scripts.test_cases;

import bases.Setup;
import global_values.GlobalValue;
import handles.ExcelHandle;
import keywords.UI;
import listeners.TestListener;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import test_scripts.page_objectives.LoginPage;

@Listeners(TestListener.class)
public class LoginTest extends Setup {
    LoginPage loginPage;
    ExcelHandle excelHandle;
    @BeforeMethod
    public void setup() {
        loginPage = new LoginPage();
        excelHandle = new ExcelHandle();
        excelHandle.setExcelFile("Testdata.xlsx", "LoginTestData");
        UI.openURL(GlobalValue.URL);
    }

    @Test (priority = 0, description = "Login with valid account")
    public void loginWithValidAccount() {
        setup();
        loginPage.loginWithValidAccount(
                excelHandle.getCellData("Username", 1),
                excelHandle.getCellData("Password", 1)
        );
    }

    @Test (priority = 1, description = "Login with invalid account")
    public void loginWithInvalidAccount() {
        loginPage.loginWithInvalidAccount(
                excelHandle.getCellData("Username", 2),
                excelHandle.getCellData("Password", 2)
        );
    }

    @Test (priority = 2, description = "Login without username")
    public void loginWithoutUsername() {
        loginPage.login(
                excelHandle.getCellData("Blank Data", 1),
                excelHandle.getCellData("Password", 1)
        );
    }

    @Test (priority = 3, description = "Login without password")
    public void loginWithoutPassword() {
        loginPage.login(
                excelHandle.getCellData("Username", 1),
                excelHandle.getCellData("Blank Data", 1)
        );
    }

    @Test (priority = 4, description = "Login with invalid phone number")
    public void loginWithInvalidPhoneNumber() {
        loginPage.login(
                excelHandle.getCellData("Invalid Username", 1),
                excelHandle.getCellData("Password", 1)
        );
    }
}
