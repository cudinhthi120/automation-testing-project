package test_scripts.test_cases;

import bases.Setup;
import global_values.GlobalValue;
import handles.ExcelHandle;
import keywords.UI;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import test_scripts.page_objectives.EditProfilePage;
import test_scripts.page_objectives.HomePage;
import test_scripts.page_objectives.LoginPage;
import test_scripts.page_objectives.ProfilePage;

import java.util.Arrays;

public class EditPersonalInfoTest extends Setup {
    ExcelHandle excelHandle;
    LoginPage loginPage;
    ProfilePage profilePage;
    EditProfilePage editProfilePage;
    HomePage homePage;

    @BeforeMethod
    public void setUp() {
        excelHandle = new ExcelHandle();
        loginPage = new LoginPage();
        profilePage = new ProfilePage();
        editProfilePage = new EditProfilePage();
        homePage = new HomePage();
        excelHandle.setExcelFile("Testdata.xlsx", "UserPersonalData");
        UI.openURL(GlobalValue.URL);
    }

    @Test(priority = 0)
    public void ChangeUserAccountInfoTestCase() {
        // Login to Main page
        loginPage.login("0902883759", "dinhthi3105");

        // Go to user's profile page
        homePage.goToProfilePage();

        // Go to edit user's info page
        profilePage.goToEditPage();

        // Set excel data file
        excelHandle.setExcelFile("src/main/java/testdata/Testdata.xlsx", "UserPersonalData");

        // Edit user's info
        editProfilePage.editUserPersonalInfo(
                excelHandle.getCellData("Name", 1),
                excelHandle.getCellData("Gender", 1),
                excelHandle.getCellData("Birthday", 1),
                excelHandle.getCellData("Address", 1),
                excelHandle.getCellData("Identification Number", 1),
                excelHandle.getCellData("Tax Code", 1),
                excelHandle.getCellData("Business License", 1)
        );

        Assert.assertEquals(Arrays.stream(profilePage.checkUserInfo()).toList().toString(),
                excelHandle.getRowData(1).toString());
    }
}

