package test_scripts.page_objectives;

import keywords.UI;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class ManageListingPage {
    By tabAll = By.xpath("//a[@value='0']");
    By nextBtn = By.xpath("//div[contains(text(),'Tiếp theo')]");
    By postElement = By.xpath("//div[@class='sc-1eldroe-13 FRxkn']");

    public void checkTotalItemInTab() {
        ArrayList<String> listChar = new ArrayList<>();
        String allItemInTab = getTabAllText();
        String removeParenthesesStr = allItemInTab.replaceAll("[\\[\\](){}]", "");

        for(String ch : removeParenthesesStr.split("\\s")) {
            listChar.add(ch);
        }

        int totalItemInTab = Integer.parseInt(listChar.get(2));
        int numberItemInPage = 20;
        int totalPage = totalItemInTab / numberItemInPage;
        int countTotalItem = 0;

        // If there are surplus items + 1 total page
        if(totalItemInTab % numberItemInPage > 0) {
            totalPage += 1;
        }

        for(int i=1; i<=totalPage; i++) {
            countTotalItem = countTotalItem + countItemInOnePage(i);
            if(i < totalPage) {
                clickNextBtn();
            }
        }

        UI.assertEquals(countTotalItem, totalItemInTab);
    }

    public int countItemInOnePage(int page) {
        int countItem = 0;
        List<WebElement> rows = UI.getWebElements(postElement);
        System.out.println("Số dòng dữ liệu: "+ rows.size() + " trang " + page);
        for(int i=1; i<= rows.size();i++) {
            countItem += 1;
        }

        System.out.println(countItem);
        return countItem;
    }

    public String getTabAllText() {
        return UI.getText(tabAll);
    }

    public void clickNextBtn() {
        UI.clickElement(nextBtn);
    }
}
