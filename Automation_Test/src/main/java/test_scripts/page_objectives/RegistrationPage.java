package test_scripts.page_objectives;

import keywords.UI;
import org.openqa.selenium.By;

public class RegistrationPage {
    public static By phoneFiled() {
        return By.xpath("//input[@id='phone']");
    }
    public static By passwordField() {
        return By.xpath("//input[@id='password']");
    }
    public static By acceptCheckBox() {
        return By.xpath("//input[@id='accepted']");
    }
    public static By signupBtn() {
        return By.xpath("//span[contains(text(),'Đăng ký')]");
    }
    public static By phoneFieldErrorMessage() {
        return By.xpath("//label[@for='phone']/following-sibling::span[@class='sc-1i9w91w-2 dzgEam']");
    }
    public static By passwordFieldErrorMessage() {
        return By.xpath("//label[@for='password']/following-sibling::span[@class='sc-1i9w91w-2 dzgEam']");
    }
    public static By regulationCheckBoxErrorMessage() {
        return By.xpath("//div[@class='sc-sc4voz-14 lctjAJ flex-middle']/following::span[@class='sc-1i9w91w-2 dzgEam']");
    }
    public static By verificationFormTitle() {
        return By.xpath("//div[@class='title-form']");
    }
    public static By duplicatePhoneNumberAlert() {
        return By.xpath("//div[@role='alert']");
    }

    public void register(String phone, String password) {
        inputPhone(phone);
        inputPassword(password);
        clickSignupBtn();
        UI.waitForPageLoaded();
    }

    public void registerPasswordLengthValidation(String username, String password) {
        register(username, password);
        UI.assertEquals(getPasswordFieldErrorMessage(),
                "Mật khẩu phải từ 6 - 50 ký tự.");
    }

    public void registerUsernameLengthValidation(String username, String password) {
        register(username, password);
        UI.assertEquals(getPhoneFieldErrorMessage(),
                "Số điện thoại phải đủ 10 số và bắt đầu bằng số 0.");
    }

    public void registerWithDuplicateUserName(String username, String password) {
        register(username, password);
        UI.assertEquals(getAlertMessage(),
                "Số điện thoại này đã được đăng ký, vui lòng sử dụng số điện thoại khá");
    }

    public void registerWithInvalidPhoneNumber(String username, String password) {
        register(username, password);
        UI.assertEquals(getPhoneFieldErrorMessage(),
                "Số điện thoại phải đủ 10 số và bắt đầu bằng số 0.");
    }

    public void registerWithoutPassword(String username, String password){
        register(username, password);
        UI.assertEquals(getPasswordFieldErrorMessage(),
                "Vui lòng nhập mật khẩu.");
    }

    public void registerWithoutUsername(String username, String password) {
        register(username, password);
        UI.assertEquals(getPhoneFieldErrorMessage(),
                "Vui lòng nhập số điện thoại.");
    }
    public void registerWithValidInfo(String username, String password) {
        register(username, password);
        UI.assertEquals(getVerificationFormTitle(),
                "Xác nhận số điện thoại");
    }

    public void inputPhone(String text) {
        UI.setText(phoneFiled(), text);
    }

    public void inputPassword(String text) {
        UI.setText(passwordField(), text);
    }

    public void tickAcceptCheckBox() {
        boolean isChecked = UI.isSelected(acceptCheckBox());
        if(!isChecked) {
            UI.clickElement(acceptCheckBox());
        }
    }

    public void unTickAcceptCheckBox() {
        boolean isChecked = UI.isSelected(acceptCheckBox());
        if(isChecked) {
            UI.clickElement(acceptCheckBox());

        }
    }

    public void clickSignupBtn() {
        UI.clickElement(signupBtn());
    }

    public String getPhoneFieldErrorMessage() {
        UI.waitForElementVisible(phoneFieldErrorMessage());
        return UI.getText(phoneFieldErrorMessage());
    }

    public String getPasswordFieldErrorMessage() {
        UI.waitForElementVisible(passwordFieldErrorMessage());
        return UI.getText(passwordFieldErrorMessage());
    }

    public String getRegulationCheckBoxErrorMessage() {
        return UI.getText(regulationCheckBoxErrorMessage());
    }

    public String getAlertMessage() {
        UI.waitForElementVisible(duplicatePhoneNumberAlert());
        return UI.getText(duplicatePhoneNumberAlert());
    }

    public String getVerificationFormTitle() {
        return UI.getText(verificationFormTitle());
    }

}
