package test_scripts.page_objectives;

import org.openqa.selenium.By;

public class CreatePostPage {
    public static By cityElement(String element) {
        return By.xpath("//div[@class='sc-12qow75-2 kEyocY']//p[contains(text(),'" + element + "')]");
    }
    public static By districtElement(String element) {
        return By.xpath("//div[@class='sc-12qow75-2 kEyocY']//p[contains(text(),'" + element + "')]");
    }
    public static By wardElement(String element) {
        return By.xpath("//div[@class='sc-12qow75-2 kEyocY']//p[contains(text(),'" + element + "')]");
    }
    public static By streetElement(String element) {
        return By.xpath("//div[@class='sc-12qow75-2 kEyocY']//p[contains(text(),'" + element + "')]");
    }
    public static By categorySelectBox() {
        return By.xpath("//div[@class='sc-1lv2njq-2 cwPSUS']");
    }
    public static By fistCategoryChoice() {
        return By.xpath("//p[contains(text(),'Bất động sản')]");
    }
    public static By secondCategoryChoice() {
        return By.xpath("//p[normalize-space()='Cho thuê']");
    }
    public static By thirdCategoryChoice() {
        return By.xpath("//p[contains(text(),'Nhà trọ, Phòng trọ')]");
    }
    public static By cityDropdown() {
        return By.xpath("//div[@id='city_id']//div[@class='sc-5fp63n-5 fOhFor']");
    }
    public static By cityElements() {
        return By.xpath("//div[@class=\"sc-12qow75-2 kEyocY\"]");
    }
    public static By districtDropdown() {
        return By.xpath("//div[@id='district_id']//div[@class='sc-5fp63n-5 fOhFor']");
    }
    public static By wardDropdown() {
        return By.xpath("//div[@id='ward_id']//div[@class='sc-5fp63n-5 fOhFor']");
    }
    public static By streetDropdown() {
        return By.xpath("//div[@id='street_id']//div[@class='sc-5fp63n-5 fOhFor']");
    }
    public static By addressNumberField() {
        return By.xpath("//input[@name='street_number']");
    }
    public static By showAddressNumberCheckBox() {
        return By.xpath("//label[contains(text(),'Hiển thị số nhà trong tin đăng')]");
    }
    public static By livingAreaField() {
        return By.xpath("//input[@name='living_area']");
    }
    public static By priceField() {
        return By.xpath("//input[@name='price']");
    }
    public static By postTitle() {
        return By.xpath("//input[@name='title']");
    }
    public static By postDescription() {
        return By.xpath("//textarea[@name='body']");
    }
    public static By nextButton() {
        return By.xpath("//button[contains(text(),'Tiếp tục')]");
    }

//    public void createPost(
//            String city,
//            String district,
//            String ward,
//            String street,
//            String addressNumber,
//            String livingArea,
//            String price,
//            String title,
//            String description
//    )
//    {
//        openCategory();
//        selectFirstCategory();
//        selectSecondCategory();
//        selectThirdCategory();
//        openCityDropdown();
//        selectCity(city);
//        openDistrictDropdown();
//        selectDistrict(district);
//        openWardDropdown();
//        selectWard(ward);
//        openStreetDropdown();
//        selectStreet(street);
//        inputAddressNumber(addressNumber);
//        selectShowAddress();
//        inputLivingArea(livingArea);
//        inputPrice(price);
//        inputTitle(title);
//        inputDescription(description);
//        clickNextButton();
//        UI.waitForPageLoaded();
//        Assert.assertTrue(UI.getCurrentURL("/mua-dich-vu"), "This is not buy services page");
//    }

//    public static void openCategory() {
//        UI.clickElement(categorySelectBox);
//    }
//
//    public void selectFirstCategory() {
//        UI.clickElement(fistCategoryChoice);
//    }
//
//    public void selectSecondCategory() {
//        UI.clickElement(secondCategoryChoice);
//    }
//
//    public void selectThirdCategory() {
//        UI.clickElement(thirdCategoryChoice);
//    }
//
//    public void openCityDropdown() {
//        UI.clickElement(cityDropdown);
//    }
//
//    public void selectCity(String element) {
//        cityElements = By.xpath("//div[@class='sc-12qow75-2 kEyocY']//p[contains(text(),'" + element + "')]");
//        UI.clickElement(cityElements);
//    }
//
//    public void openDistrictDropdown() {
//            UI.clickElement(districtDropdown);
//    }
//
//    public void selectDistrict(String element) {
//        districtElements = By.xpath("//div[@class='sc-12qow75-2 kEyocY']//p[contains(text(),'" + element + "')]");
//        UI.clickElement(districtElements);
//    }
//
//    public void openWardDropdown() {
//        UI.clickElement(wardDropdown);
//    }
//
//    public void selectWard(String element) {
//        wardElements = By.xpath("//div[@class='sc-12qow75-2 kEyocY']//p[contains(text(),'" + element + "')]");
//        UI.clickElement(wardElements);
//    }
//
//    public void openStreetDropdown() {
//        UI.clickElement(streetDropdown);
//    }
//
//    public void selectStreet(String element) {
//        streetElements = By.xpath("//div[@class='sc-12qow75-2 kEyocY']//p[contains(text(),'" + element + "')]");
//        UI.clickElement(streetElements);
//    }
//
//    public void inputAddressNumber(String addressNumber) {
//        UI.setText(addressNumberField, addressNumber);
//    }
//
//    public void selectShowAddress() {
//        UI.clickElement(showAddressNumberCheckBox);
//    }
//
//    public void inputLivingArea(String livingArea) {
//        UI.setText(livingAreaField, livingArea);
//    }
//
//    public void inputPrice(String price) {
//        UI.setText(priceField, price);
//    }
//
//    public void inputTitle(String title) {
//        UI.setText(postTitle, title);
//    }
//
//    public void inputDescription(String description) {
//        UI.setText(postDescription, description);
//    }
//
//    public void clickNextButton() {
//        UI.clickElement(nextButton);
//    }

}
