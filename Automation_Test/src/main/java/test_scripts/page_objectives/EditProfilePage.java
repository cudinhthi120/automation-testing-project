package test_scripts.page_objectives;

import keywords.UI;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class EditProfilePage {
    public static By nameField() {
        return By.xpath("//input[@name='full_name']");
    }
    public static By genderRadioButtonList() {
        return By.xpath("//input[@name='gender']");
    }
    public static By birthdayField(){
        return By.xpath("//input[@placeholder='Nhập ngày tháng năm sinh (dd/mm/yyyy)']");
    }
    public static By addressField(){
        return By.xpath("//input[@name='address']");
    }
    public static By identificationCardField() {
        return By.xpath("//input[@name='code']");
    }
    public static By taxCodeField() {
        return By.xpath("//input[@name='tax_code']");
    }
    public static By businessLicenseField() {
        return By.xpath("//input[@name='business_license']");
    }
    public static By updateButton() {
        return By.xpath("//button[@type='submit']");
    }

    public void editUserPersonalInfo(
            String name,
            String gender,
            String birthDay,
            String address,
            String identificationNumber,
            String taxCode,
            String businessLicense
    ) {
        inputName(name);
        selectGender(gender);
        inputBirthday(birthDay);
        inputAddress(address);
        inputIdentificationCard(identificationNumber);
        inputTaxCode(taxCode);
        inputBusinessLicense(businessLicense);
        clickUpdateButton();
        UI.waitForPageLoaded();
    }

    public void inputName(String name) {
        if(!UI.getTextByAttribute(nameField()).isEmpty()) {
            UI.deleteAllAction(nameField());
        }
        UI.setText(nameField(), name);
    }

    public void selectGender(String element) {
        List<WebElement> listGender = UI.getWebElements(genderRadioButtonList());
        int number = switch (element) {
            case "Nam" -> 0;
            case "Nữ" -> 1;
            case "Khác" -> 2;
            default -> 0;
        };
        if(number > listGender.size()) {
            UI.clickElementInList(listGender, 0);
        } else {
            UI.clickElementInList(listGender, number);
        }
    }

    public void inputBirthday(String birthDay) {
        if(!UI.getTextByAttribute(birthdayField()).isEmpty()) {
            UI.deleteAllAction(birthdayField());
        }
        UI.setText(birthdayField(), birthDay);

    }

    public void inputAddress(String address) {
        if(!UI.getTextByAttribute(addressField()).isEmpty()) {
            UI.deleteAllAction(addressField());
        }
        UI.setText(addressField(), address);
    }

    public void inputIdentificationCard(String identificationNumber) {
        if(!UI.getTextByAttribute(identificationCardField()).isEmpty()) {
            UI.selectAllAction(identificationCardField());
        }
        UI.setText(identificationCardField(), identificationNumber);
    }

    public void inputTaxCode(String taxCode) {
        if(!UI.getTextByAttribute(taxCodeField()).isEmpty()) {
            UI.deleteAllAction(taxCodeField());
        }
        UI.setText(taxCodeField(), taxCode);
    }

    public void inputBusinessLicense(String businessLicense) {
       if(!UI.getTextByAttribute(businessLicenseField()).isEmpty()) {
           UI.deleteAllAction(businessLicenseField());
       }
        UI.setText(businessLicenseField(), businessLicense);

    }

    public void clickUpdateButton() {
        UI.clickElement(updateButton());
    }

}
