package test_scripts.page_objectives;

import keywords.UI;
import org.openqa.selenium.By;

public class ProfilePage {
    public static By editButton() {
        return By.xpath("//div[@class='sc-1c7w2i5-1 jmPCem']//div[1]//div[1]//div[1]//div[2]//button[1]");
    }
    public static By nameField() {
        return By.xpath("//body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]");
    }
    public static By genderField() {
        return By.xpath("//body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]");
    }
    public static By birthdayField() {
        return By.xpath("//body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div[1]");
    }
    public static By addressField() {
        return By.xpath("//body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div[1]");
    }
    public static By identificationCardField() {
        return By.xpath("//body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[5]/div[1]/div[1]");
    }
    public static By taxCodeField() {
        return By.xpath("//body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[6]/div[1]/div[1]");
    }
    public static By businessLicenseField() {
        return By.xpath("//body[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[1]/div[7]/div[1]/div[1]");
    }

    public String[] checkUserInfo() {
        return new String[] {
            getTextInNameField(),
            getTextInGenderField(),
            getTextInBirthdayField(),
            getTextInAddressField(),
            getTextInIdentificationField(),
            getTextInTaxCodeField(),
            getTextInBusinessLicenseField()
        };
    }

    public void goToEditPage() {
        clickEditButton();
        UI.waitForPageLoaded();
    }

    public void clickEditButton() {
        UI.clickElement(editButton());
    }

    public String getTextInNameField() {
        return UI.getText(nameField());
    }

    public String getTextInGenderField() {
       return UI.getText(genderField());
    }

    public String getTextInBirthdayField() {
        return UI.getText(birthdayField());
    }

    public String getTextInAddressField() {
        return UI.getText(addressField());
    }

    public String getTextInIdentificationField() {
        return UI.getText(identificationCardField());
    }

    public String getTextInTaxCodeField() {
        return UI.getText(taxCodeField());
    }

    public String getTextInBusinessLicenseField() {
        return UI.getText(businessLicenseField());
    }
}
