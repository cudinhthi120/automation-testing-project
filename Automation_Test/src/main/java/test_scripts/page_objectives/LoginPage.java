package test_scripts.page_objectives;

import bases.Setup;
import keywords.UI;
import org.openqa.selenium.By;

public class LoginPage extends Setup {
    public static By phoneField() {
        return By.xpath("//input[@id='phone']");
    }
    public static By passwordField() {
        return By.xpath("//input[@id='password']");
    }
    public static By loginBtn() {
        return By.xpath("//span[contains(text(),'Đăng nhập')]");
    }
    public static By phoneFieldErrorMessage() {
        return By.xpath("//span[@class='sc-1i9w91w-2 dzgEam']");
    }
    public static By passwordFieldErrorMessage() {
        return By.xpath("//span[@class='sc-1i9w91w-2 hKUExA']");
    }
    public static By invalidAccountAlert() {
        return By.xpath("//div[@role='alert']");
    }
    public static By signupLinkText(){
        return By.xpath("//a[contains(text(),'Đăng ký ngay')]");
    }

    public void login(String phone, String password) {
//        UI.openURL(GlobalValue.URL);
        inputPhone(phone);
        inputPassword(password);
        clickLoginBtn();
        UI.waitForPageLoaded();
    }

    public void loginNormal(String name, String password) {

    }

    public void goToRegisterPage() {
        clickSignupBtn();
        UI.waitForPageLoaded();
    }

    public void loginWithInvalidPhoneNumber(String phone, String password) {
        login(phone, password);
        UI.assertEquals(getPhoneFieldErrorMessage(), "Số điện thoại phải đủ 10 số và bắt đầu bằng số 0.");
    }

    public void loginWithInvalidAccount(String phone, String password) {
        login(phone, password);
        UI.assertEquals(getInvalidAlertText(), "Không tìm thấy thông tin tài khoản theo số điện thoại này");
    }

    public void loginWithoutPassword(String phone, String password) {
        login(phone, password);
        UI.assertEquals(getPasswordFieldErrorMessage(), "Vui lòng nhập mật khẩu.");
    }

    public void loginWithoutUsername(String phone, String password) {
        login(phone, password);
        UI.assertEquals(getPhoneFieldErrorMessage(), "Vui lòng nhập số điện thoại.");
    }

    public void loginWithValidAccount(String phone, String password) {
        login(phone, password);
        UI.assertEquals(UI.getTitle(),
                "Mua bán đăng tin rao vặt nhanh chóng, hiệu quả 2024 - Muaban.net");
    }

    public void inputPhone(String text) {
        UI.setText(phoneField(), text);
    }

    public void inputPassword(String text) {
        UI.setText(passwordField(), text);
    }

    public void clickLoginBtn() {
        UI.clickElement(loginBtn());
    }

    public void clickSignupBtn() {
        UI.clickElement(signupLinkText());
    }

    public String getPhoneFieldErrorMessage() {
        return UI.getText(phoneFieldErrorMessage());
    }

    public String getPasswordFieldErrorMessage() {
        return UI.getText(passwordFieldErrorMessage());
    }

    public String getInvalidAlertText() {
        UI.waitForElementVisible(invalidAccountAlert());
        return UI.getText(invalidAccountAlert());
    }

}
