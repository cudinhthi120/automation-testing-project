package test_scripts.page_objectives;

import keywords.UI;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class HomePage {
    public static By createPostBtn = By.xpath("//div[@class='sc-1y7y4ai-3 dXIRWo']//button");
    public static By userInfoList = By.xpath("//button[@class=\"sc-18oo25t-9 kiHQrM\"]");
    public static By userAccountInfo = By.xpath("//p[contains(text(),'Thông tin tài khoản')]");
    public static By postManagementBtn = By.xpath("//button[contains(text(),'Quản lý tin')]");

    public String getTitle() {
        return UI.getTitle();
    }

    public static void goToCreatePostPage() {
        clickCreatePostBtn();
        UI.waitForPageLoaded();
    }

    public void goToProfilePage() {
        clickUserInfoList();
        clickUserAccountInfo();
        UI.waitForPageLoaded();
    }

    public void goToPostManagementPage() {
        clickPostManagementBtn();
        UI.waitForPageLoaded();
    }

    public static void clickCreatePostBtn() {
        UI.clickElement(createPostBtn);
    }

    public void clickUserInfoList() {
        List<WebElement> listHeaderButton = UI.getWebElements(userInfoList);
        UI.clickElementInList(listHeaderButton, 2);
    }

    public static void clickUserAccountInfo() {
        UI.clickElement(userAccountInfo);
    }

    private void clickPostManagementBtn() {
        UI.clickElement(postManagementBtn);
    }
}